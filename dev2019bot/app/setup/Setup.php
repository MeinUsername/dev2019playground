<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 11:34
 */

namespace MeinBot;


use ByJG\AnyDataset\Db\Factory;

class Setup
{
    public function createDB()
    {
        $dbPath = Config::$DB_CONNECTION_STRING;
        echo $dbPath;
        if (strpos(mb_strtolower($dbPath), 'sqlite') === 0) {
            $lFilePath = str_replace("sqlite://", "", $dbPath);
            if (file_exists($lFilePath)) {
                return;
            } else {

                /**
                 *     public $chat_id = '';
                 * public $from_id = '';
                 * public $to_id = '';
                 * public $chatFlowName = '';
                 * public $lastValue = "";
                 * public $created = 0;
                 * public $id;
                 */
                $dbDriver = Factory::getDbRelationalInstance(Config::$DB_CONNECTION_STRING);
                $dbDriver->execute('create table dataflows (
            id integer primary key  autoincrement, 
            chat_id varchar(255),
            message_id varchar(255),
            from_id varchar(255), 
            chatFlowName varchar(255), 
            lastvalue text, 
            created integer);'
                );

                $dbDriver->execute('create table events (
            id integer primary key  autoincrement, 
            event_name varchar(255),
            event_type varchar(255),
            event_date varchar(255), 
            event_creator varchar(255) null , 
            evemt_location varchar(255) null, 
            participans_id varchar(255) null, 
            created integer);'
                );

            }
        }
    }
}