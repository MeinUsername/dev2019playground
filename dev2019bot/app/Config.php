<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-12
 * Time: 13:14
 */

namespace MeinBot;


class Config
{
    static $BOT_TOKEN = "";
    static $CURRENT_URL = "https://devbot.fkraemer.de";
    static $DB_CONNECTION_STRING =  'sqlite://'.__DIR__."/../db/meinbot.sqlite";
}