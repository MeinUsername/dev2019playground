<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-15
 * Time: 06:04
 */

namespace MeinBot;


use React\EventLoop\Factory;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\TgLog;

class TelegramFascade extends BaseClass
{


    private static $instance = null;

    private $botToken;
    /**
     * @var TgLog
     */
    private $tgLog;

    /**
     * @var DataFlowRepo
     */
    private $dataflowRepo;

    public function __construct()
    {
        parent::__construct();
        $loop = Factory::create();
        $this->tgLog = new TgLog($this->botToken, new HttpClientRequestHandler($loop));
        $this->dataflowRepo = DataFlowRepo::getInstance();
    }


    public static function getInstance($aBotToken = null)
    {
        $lBotToken = $aBotToken;
        if (self::$instance === null) {
            self::$instance = new TelegramFascade();
        }

        if ($lBotToken === null) {
            $lBotToken = Config::$BOT_TOKEN;
        }
        self::$instance->setBotToken($lBotToken);

        return self::$instance;
    }


    public function sendMessage(string $aChatId, string $aText, $aDataFlowIdent = "not-set")
    {
        $this->getLog()->info("Send message $aText to chat_id $aChatId for  dataflow $aDataFlowIdent");


        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $aChatId;
        $sendMessage->text = $aText;

        $promise = $this->tgLog->performApiRequest($sendMessage);
        $promise->then(
            function ($response) {
                echo '<pre> Bot Response';
                $lEntry = DataFlowEntryHelper::create($response);
                $this->getLog()->info("Try to save " ,$lEntry);
                $this->dataflowRepo->save($lEntry);
//                $lRepo = DataFlowRepo::getInstance();
//                $lRepo->create($lEntry);
//                if ( $response instanceof Message)
//                {
//                    DataFlowEntryHelper::create($response);
//
//                }

                $stringResponse = var_export($response,true);
                $this->getLog()->info("Response from sendmessage".$stringResponse);
                var_dump($response);
                echo '</pre>';
            },
            function (\Exception $exception) {
                // Onoes, an exception occurred...
                echo 'Exception ' . get_class($exception) . ' caught, message: ' . $exception->getMessage();
            });
    }

    /**
     * @param mixed $botToken
     * @return TelegramFascade
     */
    public function setBotToken($botToken)
    {
        $this->botToken = $botToken;
        return $this;
    }
}