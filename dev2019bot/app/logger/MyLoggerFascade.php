<?php

use Apix\Log\Logger\File;

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-24
 * Time: 06:08
 */
class MyLoggerFascade
{

    private $log;

    public function __construct(File $aLog)
    {
        $this->log = $aLog;
    }


    public function info($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->info(self::interpolate($aMessage, $aObject), $aContext);
    }

    public function error($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->error(self::interpolate($aMessage, $aObject), $aContext);
    }

    public function emergency($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->emergency(self::interpolate($aMessage, $aObject), $aContext);
    }

    public function warning($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->warning(self::interpolate($aMessage, $aObject), $aContext);
    }

    public function debug($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->debug(self::interpolate($aMessage, $aObject), $aContext);
    }

    public function notice($aMessage, $aObject = null, array $aContext = array())
    {
        $this->log->notice(self::interpolate($aMessage, $aObject), $aContext);
    }


    protected function interpolate($aMessage, $aObject = null)
    {
        if ( $aObject)
        {
            return $aMessage . " =>  " . var_export($aObject, true);

        }
        else
        {
            return $aMessage;
        }
    }


}