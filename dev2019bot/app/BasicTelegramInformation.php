<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-12
 * Time: 21:11
 */

namespace MeinBot;

use unreal4u\TelegramAPI\Telegram\Types\User;

class BasicUserInfo
{
    public $id;
    public $firstname;
    public $lastname;

}


class BasicTelegramInformation
{
    /**
     * @var string
     */
    public $chatId;
    /**
     * @var BasicUserInfo
     */
    public $from;
    /**
     * @var string
     */
    public $type;

    /**
     *
     * @param $aJsonDecode json_deocde of raw string
     *
     * Something like
     * {
    "update_id":263178258,
    "message":{
    "message_id":49,
    "from":{
    "id":745658434,
    "is_bot":false,
    "first_name":"Florian",
    "language_code":"de"
    },
    "chat":{
    "id":745658434,
    "first_name":"Florian",
    "type":"private"
    },
    "date":1547310410,
    "text":"@DevBot2019 What is /help?",
    "entities":[
    {
    "offset":0,
    "length":11,
    "type":"mention"
    },
    {
    "offset":20,
    "length":5,
    "type":"bot_command"
    }
    ]
    }
    }
     */
    public function createFromWebhookPayload(array $aJsonDecode)
    {
        $chatId  = $aJsonDecode["message"]["chat"]["id"];
        $updateId = $aJsonDecode["update_id"];
        $lMessage = $aJsonDecode["message"];
        $lMessageFrom = $lMessage["from"];

        $fromName = $lMessageFrom["first_name"];
        $fromId = $lMessageFrom["id"];

        $lText = $lMessage['text'];
        $lEntities = $lMessage['entities'][0];
        $lEntityType = $lEntities['type'];

//        $this->from = new User()

    }
}