<?php

use MeinBot\BaseClass;

/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-19
 * Time: 21:42
 */

namespace MeinBot;

class TextlineManager extends BaseClass
{

    /**
     * @var array
     */
//    private $message = [
//        "suggest-date-ask-date" => "Welches Datum würdest du vorschlagen?"
//    ]
//;

    private $message;

    public function __construct($aLanguage = 'de')
    {
        parent::__construct();
        $lFilename = __DIR__."/$aLanguage.php";
        $this->getLog()->info("Load language file ".$lFilename);
        $this->message = include ($lFilename);
        $lMessagesLength = count($this->message);
        $this->getLog()->info("Load $lMessagesLength messages");


    }

    public function getLineById($aId)
    {
        $isInArray = array_key_exists($aId, $this->message);
        if ($isInArray)
        {
            return $this->message[$aId];
        }
        else{
            return $aId;
        }
    }

    public function getIdByLine($aId)
    {
        return array_search ($aId, $this->message);
    }
}