<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-20
 * Time: 11:18
 */

use MeinBot\TextlineManager;
use PHPUnit\Framework\TestCase;

class TextlineManagerTest extends TestCase
{
    private $testId = "suggest-date-ask-date";
    private $testValue = "Welches Datum würdest du vorschlagen?";
    private $testLang = "de";


    public function testGetLineById()
    {
        $lTextManager = new TextlineManager($this->testLang);
        $lResult = $lTextManager->getLineById($this->testId);
        $this->assertEquals($this->testValue,$lResult);

        $lNotExistingId = "blablub";
        $lResult2 = $lTextManager->getLineById($lNotExistingId);
        $this->assertEquals($lNotExistingId,$lResult2);


    }

    public function testGetIdByLine()
    {
        $lTextManager = new TextlineManager($this->testLang);
        $lResult = $lTextManager->getIdByLine($this->testValue);
        $this->assertEquals($this->testId,$lResult);

        $lNotExistingValue = "basdakdsj";
        $lResult2 = $lTextManager->getIdByLine($lNotExistingValue);
        $this->assertEquals(false,$lResult2);

    }
}
