<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-19
 * Time: 21:48
 */
return [
    "suggest-date-ask-date" => "Welches Datum würdest du vorschlagen?",
    "suggest-date-ask-type"=> "Welches Art von Event ist es?",
    "suggest-date-eventtype-kochabend" => "Kochabend",
    "suggest-date-eventtype-sonstiges" => "Sonstiges",
    "suggest-date-confirm"=> "Stimmen diese Angaben?",
    "suggest-date-yes" => "Klar!",
    "suggest-date-no" => "Hmm... irgendwas ist falsch!",
    "suggest-date-success" => "Die Mitglieder der Gruppe können dem Event nun beitreten.",
    "suggest-date-error" => "Entschuldigung. Irgendwas habe ich nicht verstanden. Magst du es nochmal versuchen?",
    "help-default" => "Wie kann ich helfen?"
];
