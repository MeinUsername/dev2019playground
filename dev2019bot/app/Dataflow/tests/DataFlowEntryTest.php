<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 10:43
 */

namespace MeinBot;

use PHPUnit\Framework\TestCase;
use MeinBot\DataFlowEntry;

class DataFlowEntryTest extends TestCase
{

    /**
     *
     */
    public function test__construct()
    {
        $test_chat_id = "chat_id_1";
        $test_from = "from_1";
        $test_to = "to_1";
        $test_flow_name = "test_flow_name";
        $test_last_value = "bkllasjdlka sdalsjdlakjds kadsjlajsd lksajdk lajdsäü~kl jaslksd lkjasl k :-)";
        $craetedTime = time();
        $subject = new DataFlowEntry($test_chat_id, $test_from, $test_to, $test_flow_name, $test_last_value);
        $this->assertEquals($test_chat_id, $subject->chat_id);
        $this->assertEquals($test_from, $subject->from_id);
        $this->assertEquals($test_to, $subject->message_id);
        $this->assertEquals($test_flow_name, $subject->chatFlowName);
        $this->assertEquals(abs($craetedTime - $subject->created) <= 1, true);
    }
}
