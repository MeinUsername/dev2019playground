<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 11:28
 */

namespace MeinBot;

use PHPUnit\Framework\TestCase;

class DataFlowRepoTest extends TestCase
{

    private $test_chat_id= "chat_id_1";

    public function testCreate()
    {
        ;
        $this->test_chat_id;
        $test_from = "from_1";
        $test_to = "to_1";
        $test_flow_name = "test_flow_name";
        $test_last_value = "bkllasjdlka sdalsjdlakjds kadsjlajsd lksajdk lajdsäü~kl jaslksd lkjasl k :-)";
        $subject = DataFlowEntry::create($this->test_chat_id, $test_to, $test_from, $test_flow_name, $test_last_value);

        $repo = new DataFlowRepo();
        $repo->save($subject);

        $entry = $repo->getById($subject->id);
        $this->assertEquals( $subject->chatFlowName,$entry->chatFlowName);
        $this->assertEquals( $subject->lastValue,$entry->lastValue);

    }



    public function test_query_and_delte()
    {
        $repo = new DataFlowRepo();
        $this->test_chat_id;
        $test_from = "from_1";
        $test_to = "to_1";
        $test_flow_name = "test_flow_name";
        $test_last_value = "bkllasjdlka sdalsjdlakjds kadsjlajsd lksajdk lajdsäü~kl jaslksd lkjasl k :-)";
        $subject = DataFlowEntry::create($this->test_chat_id, $test_to, $test_from, $test_flow_name, $test_last_value);
        $repo->save($subject);

        $allChats = $repo->getByChatId($this->test_chat_id);
        $this->assertGreaterThanOrEqual(1,count($allChats));
        foreach ($allChats as $lItem) {
            if ( $lItem instanceof DataFlowEntry)
            {
//                $repo->delete([intval($lItem->id)]);
                $repo->delete($lItem->id);
            }
        }

        $allChatsAfterDelete = $repo->getByChatId($this->test_chat_id);
        $this->assertEquals(0,count($allChatsAfterDelete));

        if ( true)
        {
            $allChats = $repo->getByChatId("");

            foreach ($allChats as $lItem) {
                if ( $lItem instanceof DataFlowEntry)
                {
//                $repo->delete([intval($lItem->id)]);
                    $repo->delete($lItem->id);
                }
            }
        }
    }
}
