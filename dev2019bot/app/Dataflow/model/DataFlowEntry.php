<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 10:30
 */

namespace MeinBot;


class DataFlowEntry
{
    public $chat_id = '';
    public $from_id = '';
    public $message_id = '';
    public $chatFlowName = '';
    public $lastValue = "";
    public $created = 0;
    public $id;

    public static function create(string $chat_id, string $message_id, string $from_id, string $chatFlowName, string $lastValue)
    {

        $value = new DataFlowEntry();
        $value->created = time();
        $value->chat_id = $chat_id;
        $value->message_id = $message_id;
        $value->from_id = $from_id;
        $value->chatFlowName = $chatFlowName;
        $value->lastValue = $lastValue;
        return $value;

    }


}