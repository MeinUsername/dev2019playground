<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 13:00
 */

namespace MeinBot;


use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\Telegram\Types\Update;

class DataFlowEntryHelper
{
    private static function processMessage(Message $aMessage, $aChatFlowName)
    {

        $lChatId = $aMessage->chat->id;
        $lFromId = $aMessage->from->id;
        $lLastValue = $aMessage->text;
        $lChatFlowName = $aChatFlowName;
        $lMessageId = $aMessage->message_id;
        $result = DataFlowEntry::create($lChatId, $lMessageId, $lFromId, $lChatFlowName, $lLastValue);
        return $result;
    }


    public static function create($aUpdate, $chatFlowName = "default")
    {


        if ($aUpdate instanceof Update) {
            if ($aUpdate->message) {
                return self::processMessage($aUpdate->message, $chatFlowName);
            }

            if ($aUpdate->callback_query) {
                return self::processCallbackQuery($aUpdate->callback_query, $chatFlowName);
            }
        }

        if ($aUpdate instanceof Message) {
            return self::processMessage($aUpdate, $chatFlowName);
        }


    }

    private static function explode_ChatflowName($aValue)
    {
        $delimter = "@@";
        $split = explode($delimter, $aValue);
        if (count($split) == 2) {
            return ['chatflowname' => $split[0], 'value' => $split[1]];

        } else {
            return $aValue;
        }
    }


    public static function implode_ChatflowName($aChatflowName, $aValue)
    {
        $lDelimter = "@@";
        return $aChatflowName . $lDelimter . $aValue;
    }


    private static function processCallbackQuery(\unreal4u\TelegramAPI\Telegram\Types\CallbackQuery $callback_query, string $chatFlowName)
    {
        $lChatFlowName = $chatFlowName;
        $lResult = new DataFlowEntry();
        $lChatId = $callback_query->message->chat->id;
        $lMessageId = $callback_query->message->message_id;
        $lLastValue = $callback_query->data;

        $lFrom = $callback_query->from->id;
        if ($lLastValue) {
            $lSplit = self::explode_ChatflowName($lLastValue);
            if ($lSplit['chatflowname']) {
                $lChatFlowName = $lSplit['chatflowname'];
                $lLastValue = $lSplit['value'];
            }

            $lSplit = explode("@@", $lLastValue);
            if (count($lSplit) == 2) {
                $lChatFlowName = $lSplit[0];
                $lLastValue = $lSplit[1];
            }
        }
        $lResult = DataFlowEntry::create($lChatId, $lMessageId, $lFrom, $lChatFlowName, $lLastValue);
        return $lResult;

    }
}