<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-13
 * Time: 11:15
 */

namespace MeinBot;


use ByJG\AnyDataset\Db\Factory;
use ByJG\MicroOrm\Exception\InvalidArgumentException;
use ByJG\MicroOrm\Exception\OrmModelInvalidException;
use ByJG\MicroOrm\Repository;

class DataFlowRepo extends BaseClass
{

    private $dbDriver;
    const TABLE_NAME = 'dataflows';

    private static $instance = null;


    public static function getInstance() : DataFlowRepo
    {

        if (self::$instance === null) {
            self::$instance = new DataFlowRepo();

        }

        return self::$instance;
    }

    private function getMapper()
    {
        try {
            $mapper = new \ByJG\MicroOrm\Mapper(
                DataFlowEntry::class,   // The full qualified name of the class
                self::TABLE_NAME,        // The table that represents this entity
                'id'            // The primary key field
            );
            return $mapper;
        } catch (OrmModelInvalidException $e) {
            $this->getLog()->error("Error createting Mapper for DataFlow: " . $e->getMessage());
        }
    }

    /**
     * @return Repository
     */
    private function getRepo()
    {
        $this->getLog()->info("Call to getRepo");
        $this->dbDriver = Factory::getDbRelationalInstance(Config::$DB_CONNECTION_STRING);
        if ( $this->dbDriver)
        {
            $this->getLog()->info("Created dbDriver@getRepo");

        }
        $repo = new Repository($this->dbDriver, $this->getMapper());
        if ($repo)
        {
            $this->getLog()->info("Created repo@getRepo");

        }
//        $repository = new \ByJG\MicroOrm\Repository($dataset, $this->getMapper());

        return $repo;
    }

    public function save(DataFlowEntry $entry)
    {
        try {

            $this->getRepo()->save($entry);
        } catch (\Exception $e) {
            $this->getLog()->error("Exception in creating data-flow entry: " . $e->getMessage());
        }
    }

    public function getById($id)
    {
        return $this->getRepo()->get($id);
    }

    /**
     * @param string $aChatId
     * @return array|null
     */
    public function getByChatId(string $aChatId)
    {
        try {
            $query = \ByJG\MicroOrm\Query::getInstance()
                ->table(self::TABLE_NAME)
                ->orderBy(["created asc"])
                ->where('chat_id=:cid', ['cid' => $aChatId]);
            return $this->getRepo()->getByQuery($query);
        } catch (InvalidArgumentException $e) {
        } catch (\ByJG\Serializer\Exception\InvalidArgumentException $e) {
            $this->getLog()->error($e);
            return null;
        }
    }

    public function delete($aId)
    {
        try {
            $this->getRepo()->delete($aId);
        } catch (InvalidArgumentException $e) {
            $this->getLog()->error("Exception: " . $e->getMessage());
        }


    }

}