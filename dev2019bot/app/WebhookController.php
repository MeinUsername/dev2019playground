<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-12
 * Time: 13:11
 */

namespace MeinBot;


use Amp\Process\Process;
use Siler\Http\Request;


use \React\EventLoop\Factory;
use \unreal4u\TelegramAPI\HttpClientRequestHandler;
use \unreal4u\TelegramAPI\Telegram\Methods\GetUpdates;
use \unreal4u\TelegramAPI\Abstracts\TraversableCustomType;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\Telegram\Types\Update;
use \unreal4u\TelegramAPI\TgLog;
use \unreal4u\TelegramAPI\Telegram\Methods\SetWebhook;

class WebhookController extends BaseClass
{
    private function multi_implode($array, $glue = " ")
    {
        $ret = '';

        foreach ($array as $item) {
            if (is_array($item)) {
                $ret .= $this->multi_implode($item, $glue) . $glue;
            } else {
                $ret .= $item . $glue;
            }
        }

        $ret = substr($ret, 0, 0 - strlen($glue));

        return $ret;
    }


    public function determineLastStep(Update $aUpdate)
    {
        $lDataFlowHelper = DataFlowEntryHelper::create($aUpdate);
        $lRepo = DataFlowRepo::getInstance();
        $lGetAllChats = $lRepo->getByChatId($lDataFlowHelper->chat_id);
        if ( Count($lGetAllChats)> 0)
        {
            $lLastChatEntry = $lGetAllChats[0];
            $this->getLog()->info("Get last chat entry ".var_export($lLastChatEntry,true));
            if ( $lLastChatEntry instanceof DataFlowEntry)
            {
                return $lLastChatEntry->chatFlowName;
            }


        }
        else
        {
            return 'unknown';
        }
    }

    public function receive(array $routeParams)
    {


//        $this->getLog()->info("Called Webhook Receive with parameter", $routeParams);

        $raw = Request\raw();
        $this->getLog()->debug("Called Webhook receive with payload " . $raw);


        $jsonDecode = json_decode($raw, true);
        $lUpdateObject = new Update($jsonDecode);
        $lDataFlowEntry = DataFlowEntryHelper::create($lUpdateObject);
//        $lRepo = DataFlowRepo::getInstance();
        $this->getLog()->info("Try to save from webhook".var_export($lDataFlowEntry,true));
//        $lRepo->save($lDataFlowEntry);
        $this->getLog()->info("DataFlowEntry: ". var_export($lDataFlowEntry,true));
//        $this->getLog()->info($lUpdateObject->message->from->first_name);
//        $lEntityType = $lUpdateObject->message->entities[0]->type;

        $lUpdateProcessor = new UpdateProcessor();
        $lUpdateProcessor->processUpdate($lUpdateObject);
//        $this->getLog()->info("Got entity type $lEntityType");

//        if ($lEntityType === "bot_command") {
//            $this->process_command($lUpdateObject);
//
////            $this->sendMessage("Hello Chatter $fromName! Your commend is $lText", $chatId);
//        } else {
//            $lLastStep = $this->determineLastStep($lUpdateObject);
//            $this->getLog()->info("Did not find a command");
////            $this->sendMessage("Hello Chatter $fromName! What do you mean with $lText", $chatId);
//        }

//        "message":{"message_id":41,"from":{"id":745658434,"is_bot":false,"first_name":"Florian","language_code":"de"},"chat":{"id":745658434,"first_name":"Florian","type":"private"},"date":1547309756,"text":"/help","entities":[{"offset":0,"length":5,"type":"bot_command"}]}}

//        $this->getLog()->info("Called Webhook receive with payload ", [ $jsonDecode ] );
//        $this->getLog()->info("Called Webhook receive with payload ". $jsonDecode["message"]["chat"]["id"] );
//        $this->getLog()->info("Called Webhook receive with payload ".implode($raw) );


    }

//    public function sendMessage(string $aMessageText, string $aChatId)
//    {
//        $this->getLog()->info("Send message to chat $aChatId");
//        $loop = Factory::create();
//        $tgLog = new TgLog(Config::$BOT_TOKEN, new HttpClientRequestHandler($loop));
//        $sendMessage = new SendMessage();
//        $sendMessage->chat_id = $aChatId;
//        $sendMessage->text = $aMessageText;
//        $tgLog->performApiRequest($sendMessage);
//        $loop->run();
//    }

    public function register()
    {

        $token = Config::$BOT_TOKEN;
        $webhookUrl = Config::$CURRENT_URL . '/webhook-receive';
        $loop = Factory::create();

        $setWebhook = new SetWebhook();
        $setWebhook->url = $webhookUrl;
        $tgLog = new TgLog($token, new HttpClientRequestHandler($loop));
        $tgLog->performApiRequest($setWebhook);
        $this->getLog()->info("Called Webhook Register to " . $webhookUrl);

        $loop->run();

    }
}