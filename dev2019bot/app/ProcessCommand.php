<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-12
 * Time: 21:05
 */

namespace MeinBot;

use React\EventLoop\Factory;
use SebastianBergmann\CodeCoverage\Report\Text;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\ForceReply;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Update;
use unreal4u\TelegramAPI\TgLog;

class CommandChatFlow
{
    public static $suggestDate = "suggest_date";
    public static $joinEvent = "joinEvent";
    public static $suggestMeal = "suggestMeal";
    public static $cancelEvent = "cancelEvent";
}

class ProcessCommand extends BaseClass
{

    /**
     * @var DataFlowRepo
     */
    private $dataflow_repo;
    /**
     * @var TextlineManager
     */
    private $textManager;

    public function __construct($aRepo = null, $aTextLineManager = null)
    {
        parent::__construct();
        if ($aRepo !== null)
        {
            $this->dataflow_repo = $aRepo;
        }
        else
        {
            $this->dataflow_repo = DataFlowRepo::getInstance();
        }

        if ( $aTextLineManager !== null)
        {
           $this->textManager = $aTextLineManager;
        }
        else
        {
            $this->textManager = new TextlineManager('de');
        }


    }

    public function suggestDate(Update $aUpdate)
    {
        $this->getLog()->info("Process suggestDate command");
        $lFromFirstname = $aUpdate->message->from->first_name;
        $lToken = Config::$BOT_TOKEN;
        $this->getLog()->info("Process suggestDate from $lFromFirstname");
        $loop = Factory::create();


        $repo = DataFlowRepo::getInstance();

        $dataflow_entry = DataFlowEntryHelper::create($aUpdate, CommandChatFlow::$suggestDate);
        $repo->save($dataflow_entry);
        var_dump($dataflow_entry);


        $tgLog = new TgLog($lToken, new HttpClientRequestHandler($loop));
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $aUpdate->message->chat->id;
        $lTextManager  = new TextlineManager('de');
        $lMessage = $lTextManager->getLineById("suggest-date-ask-date");


        $sendMessage->text = $lMessage;
        $lForaceReply = new ForceReply();
        $lForaceReply->force_reply = true;
        $lForaceReply->selective = true;
        $inlineKeyboard = new Markup();

//        for ($i = 1; $i < 10; $i++) {
//            $inlineKeyboardButton = new Button();
//            $inlineKeyboardButton->text = (string)$i;
//            $inlineKeyboardButton->callback_data = 'k='.(string)$i;
//            $row[] = $inlineKeyboardButton;
//            if (count($row) > 2) {
//                $inlineKeyboard->inline_keyboard[] = $row;
//                $row = null;
//            }
//        }

        $sendMessage->disable_web_page_preview = true;
        $sendMessage->parse_mode = 'Markdown';
        $sendMessage->reply_markup = $lForaceReply;


        $promise = $tgLog->performApiRequest($sendMessage);
        $promise->then(
            function ($response) {
                $lRepo = DataFlowRepo::getInstance();
                $lEntry = DataFlowEntryHelper::create($response, CommandChatFlow::$suggestDate);
                $lRepo->save($lEntry);


                $stringResponse = var_export($response, true);
                $this->getLog()->info("Response from sendmessage" . $stringResponse);
            },
            function (\Exception $exception) {
                // Onoes, an exception occurred...
                echo 'Exception ' . get_class($exception) . ' caught, message: ' . $exception->getMessage();
            }
        );
        $loop->run();
    }

    public function determineLastStep(Update $aUpdate)
    {
        $lDataFlowHelper = DataFlowEntryHelper::create($aUpdate);
        $lGetAllChats = $this->dataflow_repo->getByChatId($lDataFlowHelper->chat_id);
       if ( Count($lGetAllChats)> 0)
       {
           $lLastChatEntry = $lGetAllChats[0];
           $this->getLog()->info("Get last chat entry ".var_export($lLastChatEntry,true));
           if ( $lLastChatEntry instanceof DataFlowEntry)
           {
               return $lLastChatEntry->chatFlowName;
           }


       }
       else
       {
           return 'unknown';
       }
    }
}