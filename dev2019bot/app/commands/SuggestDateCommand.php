<?php


/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-19
 * Time: 22:44
 */

namespace MeinBot;

use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\ForceReply;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Update;

//use unreal4u\TelegramAPI\Telegram\Types\Update;


class SuggestDateCommand extends AbstractCommand
{

    private $STEP1_TEXT_ID = "suggest-date-ask-date";
    private $STEP2_TEXT_ID = "suggest-date-ask-type";
    private $STEP3_TEXT_ID = "suggest-date-confirm";

    public function __construct($aRepo = null, $aTextLineManager = null)
    {
        parent::__construct($aRepo, $aTextLineManager);
        $this->setCommandName("suggest_date")->setCommandText("/suggest_date");

    }

    public function process(Update $aUpdate)
    {
        $this->getLog()->info("Process suggestDate command");
        $lFromFirstname = $aUpdate->message->from->first_name;
        $lToken = Config::$BOT_TOKEN;
        $this->getLog()->info("Process suggestDate from $lFromFirstname");


        $dataflow_entry = DataFlowEntryHelper::create($aUpdate, CommandChatFlow::$suggestDate);
        $this->dataflow_repo->save($dataflow_entry);


        $sendMessage = new SendMessage();
        $sendMessage->chat_id = $aUpdate->message->chat->id;
        $lTextManager = new TextlineManager('de');
        $lMessage = $lTextManager->getLineById("suggest-date-ask-date");
        $sendMessage->text = $lMessage;
        $lForaceReply = new ForceReply();
        $lForaceReply->force_reply = true;
        $lForaceReply->selective = true;
        $inlineKeyboard = new Markup();

//        for ($i = 1; $i < 10; $i++) {
//            $inlineKeyboardButton = new Button();
//            $inlineKeyboardButton->text = (string)$i;
//            $inlineKeyboardButton->callback_data = 'k='.(string)$i;
//            $row[] = $inlineKeyboardButton;
//            if (count($row) > 2) {
//                $inlineKeyboard->inline_keyboard[] = $row;
//                $row = null;
//            }
//        }

        $sendMessage->disable_web_page_preview = true;
        $sendMessage->parse_mode = 'Markdown';
        $sendMessage->reply_markup = $lForaceReply;
        $this->sendMessage($sendMessage, CommandChatFlow::$suggestDate);
    }

    public function askEventName(Update $aUpdate)
    {
        $this->getLog()->info("Try to ask question for event type");
        $this->getLog()->info("Process suggestDate command");
        $lFromFirstname = $aUpdate->message->from->first_name;
        $lToken = Config::$BOT_TOKEN;
        $this->getLog()->info("Process suggestDate from $lFromFirstname");
//        $dataflow_entry = DataFlowEntryHelper::create($aUpdate, CommandChatFlow::$suggestDate);
//        $this->getLog()->info("Create dataflow entry" . var_export($dataflow_entry, true));
//        $this->dataflow_repo->save($dataflow_entry);

        $sendMessage = new SendMessage();
        $lChatId = $aUpdate->message->chat->id;
        $this->getLog()->info( "Found chat id ".$lChatId);
        $sendMessage->chat_id = $lChatId;
        $lMessage = $this->textManager->getLineById("suggest-date-ask-type");
        $sendMessage->text = $lMessage;
//        $lForaceReply = new ForceReply();
//        $lForaceReply->force_reply = true;
//        $lForaceReply->selective = true;
//        $inlineKeyboard = new Markup();

        $keyboardOptions = [
            $this->textManager->getLineById("suggest-date-eventtype-kochabend") => CommandChatFlow::$suggestDate.'@@'.$this->textManager->getLineById("suggest-date-eventtype-kochabend"),
            $this->textManager->getLineById("suggest-date-eventtype-sonstiges") => CommandChatFlow::$suggestDate.'@@'.$this->textManager->getLineById("suggest-date-eventtype-sonstiges"),

        ];
        $lKeyboard = $this->buildKeyboard($keyboardOptions);
        $this->getLog()->info("Create keyboard entry" , $lKeyboard);

        $sendMessage->disable_web_page_preview = true;
        $sendMessage->parse_mode = 'Markdown';
        var_dump($lKeyboard);
        $sendMessage->reply_markup = $lKeyboard;
        $this->sendMessage($sendMessage, CommandChatFlow::$suggestDate);
    }

    public function determineNextAction(Update $aUpdate, array $aDataFlowEntries)
    {
        $this->getLog()->info("Process lastFlowEntries " . count($aDataFlowEntries));
        $this->getLog()->info('Determine last action in suggest_date');
        var_dump($aDataFlowEntries);
        $lFoundSteps = [];
        foreach ($aDataFlowEntries as $index => $lDataFlowEntry) {
            if ($lDataFlowEntry instanceof DataFlowEntry) {

                $this->getLog()->info("Process $lDataFlowEntry->lastValue");
                $lLastValueId = $this->textManager->getIdByLine($lDataFlowEntry->lastValue);
                $this->getLog()->info("Get LastvalueId" . var_export($lLastValueId));
                if ($lLastValueId) {
                    if ($lLastValueId === $this->STEP1_TEXT_ID) {
                        $lFoundSteps[] = 1;
                        $this->getLog()->info("Found step1 dataflowentry");
                    }
                    if ($lLastValueId === $this->STEP2_TEXT_ID) {
                        $lFoundSteps[] = 2;
                        $this->getLog()->info("Found step2 dataflowentry");
                    }
                    if ($lLastValueId === $this->STEP3_TEXT_ID) {
                        $lFoundSteps[] = 3;
                        $this->getLog()->info("Found step3 dataflowentry");
                    }


                } else {
                    $this->getLog()->info("Could not find lastvalueid of " . $lDataFlowEntry->lastValue);
                }
            }
        }

        $this->getLog()->info("Found steps array" . var_export($lFoundSteps));

        if (in_array(1, $lFoundSteps)) {
            $this->getLog()->info('Found step 1');
            if (in_array(2, $lFoundSteps)) {
                if (in_array(3, $lFoundSteps)) {
                    $this->getLog()->info("Need to specify task 3");
                } else {
                    $this->getLog()->info("Need to else specify task 3");

                }
            } else {

                $this->askEventName($aUpdate);
            }
        } else {
            $this->process($aUpdate);
        }


    }

}