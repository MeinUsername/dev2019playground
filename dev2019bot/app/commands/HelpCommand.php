<?php





namespace MeinBot;

use MeinBot\CommandChatFlow;
use MeinBot\Config;
use MeinBot\DataFlowEntryHelper;
use MeinBot\DataFlowRepo;
use MeinBot\TextlineManager;
use React\EventLoop\Factory;
use unreal4u\TelegramAPI\HttpClientRequestHandler;
use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\ForceReply;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Update;
use unreal4u\TelegramAPI\TgLog;

class HelpCommand extends AbstractCommand
{

    public function __construct($aRepo = null, $aTextLineManager = null)
    {
        parent::__construct($aRepo, $aTextLineManager);
        $this->setCommandName("help")->setCommandText("/help");
    }

    public function process(Update $aUpdate)
    {
        $this->getLog()->info("Process help command");
        $loop = Factory::create();
        $tgLog = new TgLog(Config::$BOT_TOKEN, new HttpClientRequestHandler($loop));
        $sendMessage = new SendMessage();
        $lMessageText = $this->textManager->getLineById("help-default");
        $sendMessage->chat_id = $aUpdate->message->chat->id;
        $sendMessage->text = $lMessageText;
        $tgLog->performApiRequest($sendMessage);
        $loop->run();
    }


    public function determineNextAction(Update $aUpdate, array $aDataFlowEntries)
    {
        return null;
    }
}