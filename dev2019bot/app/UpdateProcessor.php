<?php

namespace MeinBot;


use unreal4u\TelegramAPI\Telegram\Types\Update;


class UpdateProcessor extends BaseClass
{
    private $supportedCommands = array();

    public function processUpdate(Update $aUpdate)
    {
        var_dump($aUpdate);
        $this->supportedCommands = array(new SuggestDateCommand(), new HelpCommand());
//        $this->supportedCommands[] = new SuggestDateCommand();
//        $this->supportedCommands[] = new HelpCommand();

        var_dump($this->supportedCommands);

//        $this->getLog()->info("SupportedCommands: ".var_export($this->supportedCommands));

        if ($this->isBotCommand($aUpdate)) {
            $lCommand = $this->determineCommand($aUpdate);
            $this->processCommand($lCommand, $aUpdate);
        } else {
            $this->getLog()->info("Try to determine next step");
            $this->determineNextStep($aUpdate);
        }
    }


    public function __construct()
    {
        parent::__construct();

    }

    private function isBotCommand(Update $aUpdate)
    {
        if ($aUpdate->message) {
            if ($aUpdate->message->entities && !empty($aUpdate->message->entities) && $aUpdate->message->entities[0]->type === "bot_command") {
                return true;
            }

        }

        return false;
    }

    private function determineCommand(Update $aUpdate)
    {
        $lLowercaseText = mb_strtolower($aUpdate->message->text);
        $lCommand = trim($lLowercaseText, "//");
        return $lCommand;
    }

    private function processCommand(string $aCommandString, Update $aUpdate)
    {
        $this->getLog()->info("Try to process command " . $aCommandString);
        /**
         * @var AbstractCommand $lCommand
         */

        $lCommand = $this->getCommandByText($aCommandString);
        var_dump($lCommand);
        if ($lCommand) {
            var_dump($lCommand);
            $lCommand->process($aUpdate);

        }

    }


    private function getCommandByText($aText)
    {
        var_dump($aText);
        /** @var AbstractCommand $lCommand */
        $lResult = null;
        $this->getLog()->info("Try o find command with commandText $aText");
        foreach ($this->supportedCommands as $lCommand) {
            if (strpos($aText, $lCommand->getCommandName()) !== false) {
                $lResult = $lCommand;
                break;
            }
        }

        return $lResult;
    }

    private function determineNextStep(Update $aUpdate)
    {
        //1. getLastCommand
        $lRepo = DataFlowRepo::getInstance();
        $lDataflowEntry = DataFlowEntryHelper::create($aUpdate);
        $this->getLog()->info("Dataflow determineNextStep " . var_export($lDataflowEntry, true));
        $lRepo->save($lDataflowEntry);
        $this->getLog()->info("Try to get updates for dataflowentry.chat_id " . $lDataflowEntry->chat_id);
        $lChatsEntries = $lRepo->getByChatId($lDataflowEntry->chat_id);
        $lCommand = null;
        var_dump($lChatsEntries);
        $lDataflowEntiresTillLastChat = [];
        foreach ($lChatsEntries as $lChatsEntry) {
            var_dump($lChatsEntry);
            $this->getLog()->info("Process dataflowsEntries till last command is found");
            array_push($lDataflowEntiresTillLastChat, $lChatsEntry);
            if ($lChatsEntry instanceof DataFlowEntry) {
                $lFoundCommand = $this->getCommandByText($lChatsEntry->lastValue);
                if ($lFoundCommand !== null) {
                    $lCommand = $lFoundCommand;

                    //push teh entries with the same timesamp to the stack
                    foreach ($lChatsEntries as $lSecondsChatEntry) {
                        if ($lSecondsChatEntry instanceof DataFlowEntry) {
                            if ($lSecondsChatEntry->created == $lChatsEntry->created) {
                                $lDataflowEntiresTillLastChat[] = $lSecondsChatEntry;
                            } else {
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }

        if ($lCommand !== null) {
            $this->getLog()->info("Found last command " . $lCommand->getCommandName());
            $lCommand->determineNextAction($aUpdate, $lDataflowEntiresTillLastChat);

        }
        //2. Call dermineNextStep of Command --> Returns certain codes
        //3. Resolve code to command or question


    }
}