<?php
/**
 * Created by PhpStorm.
 * User: florian
 * Date: 2019-01-12
 * Time: 15:17
 */




namespace MeinBot;

use Apix\Log\Logger;
use MyLoggerFascade;


class BaseClass
{

    public $log = null;

    public function __construct()
    {
    }

    /**
     * @return MyLoggerFascade
     */
    public function getLog()
    {
        if ( $this->log === null)
        {
            $lCoreLogger = new Logger\File(__DIR__ . '/../log/DevBot.log');
            $lCoreLogger->setMinLevel('warning') ;// intercept logs that are >= `warning`
            $this->log = new MyLoggerFascade($lCoreLogger);

        }
        return $this->log;
    }
}