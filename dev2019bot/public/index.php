<?php declare(strict_types=1);

use MeinBot\BaseClass;
use MeinBot\EventsController;
use MeinBot\Setup;
use MeinBot\WebhookController;
use Siler\Http\Request;

require_once __DIR__ . '/../vendor/autoload.php';


use Siler\Route;

$baseclass = new BaseClass();
$baseclass->getLog()->info("Test logger writing");

Route\get('/', function () {
    $b = new BaseClass();
    $b->getLog()->info("Receive get request to /");
    echo 'Hello World';
});

Route\any('/webhook-receive', function (array $routeParams) {
    $whc = new WebhookController();
    $whc->receive($routeParams);
});

Route\get('/webhook-register', function (array $routeParams) {
    $whc = new WebhookController();
    $whc->register();
});


Route\get('/setup', function (array $routeParams) {
    $setup = new Setup();
    $setup->createDB();
});

/** @var TYPE_NAME $EventController */
$EventController = new EventsController();


Route\post('/events', [$EventController, 'post']);